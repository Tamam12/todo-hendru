import 'package:firetest/models/user.dart';
import 'package:firetest/screens/authenticate/phone-number-verify.dart';
import 'package:firetest/screens/home/add-note.dart';
import 'package:firetest/screens/home/home.dart';
import 'package:firetest/screens/wrapper.dart';
import 'package:firetest/service/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: 'wrapper',
        routes: <String, WidgetBuilder> {
          'wrapper': (BuildContext context) => Wrapper(),
          'home': (BuildContext context) => Home(),
          'phoneauth': (BuildContext context) => PhoneVerify(),
          'addnote': (BuildContext context) => AddNote(),
        },
      ),
    );
  }
}
