import 'package:firebase_auth/firebase_auth.dart';
import 'package:firetest/service/auth.dart';
import 'package:firetest/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class PhoneVerify extends StatefulWidget {
  final Function toggleView;
  PhoneVerify({this.toggleView});

  @override
  _PhoneVerifyState createState() => _PhoneVerifyState();
}

class _PhoneVerifyState extends State<PhoneVerify> {
  String phoneNo;
  String smsOTP;
  String verificationId;
  String errorMassage = '';
  FirebaseAuth _auth = FirebaseAuth.instance;
  bool loading = false;

  Future<void> verifyPhone() async {
    final PhoneCodeSent smsOTPSent = (String verId, [int forceCodeResend]) {
      this.verificationId = verId;
      smsOTPDialog(context).then((value) {
        print('Signed In');
      });
    };

    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: this.phoneNo,
          timeout: const Duration(seconds: 20),
          verificationCompleted: (AuthCredential phoneAuthCrential) {
            print(phoneAuthCrential);
          },
          verificationFailed: (AuthException exeptio) {
            print('${exeptio.message}');
          },
          codeSent: smsOTPSent,
          codeAutoRetrievalTimeout: (String verId) {
            this.verificationId = verId;
          });
    } catch (e) {
      handleError(e);
    }
  }

  Future<bool> smsOTPDialog(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
          title: Text('Enter OTP Code'),
          content: Container(
            height: 85,
            child: Column(
              children: <Widget>[
                TextField(
                  onChanged: (value) {
                    this.smsOTP = value;
                  },
                ),
                (errorMassage != ''
                    ? Text(
                        errorMassage,
                        style: TextStyle(color: Colors.red),
                      )
                    : Container())
              ],
            ),
          ),
          contentPadding: EdgeInsets.all(10),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                _auth.currentUser().then((user) {
                  if (user != null) {
                    setState(() => loading = true);
                    Navigator.of(context).pop();
                    Navigator.of(context).pushReplacementNamed('home');
                  } else {
                    signIn();
                    loading = false;
                  }
                });
              },
              child: Text('Done'),
            )
          ],
        );
      },
    );
  }

  signIn() async {
    try {
      final AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: verificationId,
        smsCode: smsOTP,
      );
      final FirebaseUser user =
          (await _auth.signInWithCredential(credential)) as FirebaseUser;
      final FirebaseUser currentUser = await _auth.currentUser();
      assert(user.uid == currentUser.uid);
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacementNamed('home');
    } catch (e) {
      handleError(e);
    }
  }

  handleError(PlatformException error) {
    print(error);
    switch (error.code) {
      case 'ERROR_INVALID_VERIFICATION_CODE':
        FocusScope.of(context).requestFocus(new FocusNode());
        setState(() {
          errorMassage = 'Invalid Code';
        });
        Navigator.of(context).pop();
        smsOTPDialog(context).then((value) {
          print('Signed In');
        });
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.blue,
              title: Text(
                'Phone Auth',
                style: TextStyle(color: Colors.white),
              ),
              centerTitle: true,
            ),
            body: Center(
              child: Container(
                padding: EdgeInsets.all(25),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration:
                          InputDecoration(hintText: 'Enter your Number'),
                      onChanged: (val) {
                        this.phoneNo = val;
                      },
                    ),
                    (errorMassage != ''
                        ? Text(
                            errorMassage,
                            style: TextStyle(color: Colors.red),
                          )
                        : Container()),
                    SizedBox(height: 20),
                    RaisedButton(
                      color: Colors.blue,
                      onPressed: () {
                        verifyPhone();
                      },
                      child: Text('Verify'),
                      textColor: Colors.white,
                      elevation: 0,
                    ),
                    SizedBox(height: 20),
                      RaisedButton(
                        child: Text('Sign In With Google'),
                        onPressed: () async{
                          bool res = await AuthService().loginWithGoogle();
                          if (!res)
                          print('error login with google');
                        },
                      )
                  ],
                ),
              ),
            ),
          );
  }
}
