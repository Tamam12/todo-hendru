import 'package:firetest/models/note.dart';
import 'package:firetest/service/firestoreservices.dart';
import 'package:flutter/material.dart';

class AddNote extends StatefulWidget {
  final Note note;

  const AddNote({Key key, this.note}) : super(key: key);

  @override
  _AddNoteState createState() => _AddNoteState();
}

class _AddNoteState extends State<AddNote> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController _titleController;
  TextEditingController _dateController;
  TextEditingController _descController;
  FocusNode _dateNode;
  FocusNode _descNode;

  @override
  void initState() {
    super.initState();
    _titleController =
        TextEditingController(text: isEditNote ? widget.note.title : '');
    _dateController =
        TextEditingController(text: isEditNote ? widget.note.date : '');
    _descController =
        TextEditingController(text: isEditNote ? widget.note.desc : '');
    _dateNode = FocusNode();
    _descNode = FocusNode();
  }

  get isEditNote => widget.note != null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        title: Text(isEditNote ? 'Edit Note' : 'Add Note'),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Form(
          key: _key,
          child: Column(
            children: <Widget>[
              TextFormField(
                textInputAction: TextInputAction.next,
                onEditingComplete: () {
                  FocusScope.of(context).requestFocus(_dateNode);
                },
                validator: (val) {
                  if (val == null || val.isEmpty)
                    return 'Title cannot be empty';
                  return null;
                },
                controller: _titleController,
                decoration: InputDecoration(
                  labelText: 'Enter note title',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                onEditingComplete: () {
                  FocusScope.of(context).requestFocus(_descNode);
                },
                textInputAction: TextInputAction.next,
                focusNode: _dateNode,
                controller: _dateController,
                decoration: InputDecoration(
                  labelText: 'Enter note date',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 20),
              TextFormField(
                controller: _descController,
                focusNode: _descNode,
                maxLines: 5,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.3),
                child: FlatButton(
                  padding: EdgeInsets.symmetric(vertical: 13),
                  color: Colors.blue,
                  onPressed: () async {
                    if (_key.currentState.validate()) {
                      try {
                        if (isEditNote) {
                          Note note = Note(
                              date: _dateController.text,
                              title: _titleController.text,
                              desc: _descController.text,
                              id: widget.note.id);
                          FirestoreService().updateNote(note);
                        } else {
                          Note note = Note(
                            date: _dateController.text,
                            title: _titleController.text,
                            desc: _descController.text,
                          );
                          await FirestoreService().addNote(note);
                        }
                        Navigator.pop(context);
                      } catch (e) {
                        print(e);
                      }
                    }
                  },
                  textColor: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 5),
                        child: Icon(Icons.check),
                      ),
                      Text(isEditNote ? 'Update' : 'Save',
                          style: TextStyle(fontSize: 16)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
