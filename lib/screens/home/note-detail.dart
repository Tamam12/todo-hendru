import 'package:firetest/models/note.dart';
import 'package:flutter/material.dart';

class NoteDetail extends StatelessWidget {
  final Note note;

  const NoteDetail({Key key, @required this.note}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text('Detail'),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(note.title,
                  style: Theme.of(context).textTheme.title.copyWith(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      )),
              SizedBox(height: 20),
              Text(
                note.date,
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(height: 20),
              Text(note.desc),
            ],
          ),
        ),
      ),
    );
  }
}
