import 'package:firetest/models/user.dart';
import 'package:firetest/screens/authenticate/authenticate.dart';
import 'package:firetest/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);

    if (user == null) {
      return Authenticate();
    } else {
      return Home();
    }

  }
}