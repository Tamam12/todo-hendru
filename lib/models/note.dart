class Note {
  
  final String title;
  final String date;
  final String desc;
  final String id;

  Note({this.title, this.date, this.id, this.desc});

  Note.fromMap(Map<String, dynamic> data, String id):
  title = data['title'],
  date = data['date'],
  desc = data['desc'],
  id = id;
  
  Map<String, dynamic> toMap() {
    return {
      'title' : title,
      'date' : date,
      'desc' : desc
    };
  }

}